import React, {useState} from 'react';
import '../style/search.css';
import search from '../rsc/search.png';
import axios from "axios";


function Search() {

    const [searchResults, setSearchResults] = useState([]);

    const fetchResults = async () => {
        const urlName = "/api/3061607853876230/search/";
        const nameFragment = document.getElementById("search-txt").value;
        console.log(nameFragment);
        axios.get(urlName+nameFragment).then(value => {
            console.log(value)
        });
    }


    return (
        <div className="S-app">
            <div className="search-bg"></div>
            <div className="search-box">
                <input
                    id="search-txt"
                    type="text"
                    placeholder="Search your favorite hero"
                />
                <img
                    className="search-icon"
                    src={search}
                    onClick={fetchResults}></img>
            </div>
        </div>
    );
}


export default Search;
