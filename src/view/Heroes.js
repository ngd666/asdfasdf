import React, {useState, useEffect} from 'react';
import '../style/App.css';
import { BrowserRouter as Link} from 'react-router-dom';
import ReactDOM, {render} from 'react-dom'

function Heroes() {
    useEffect(() => {
        fetchHeroes();
    }, []);

    const [heroes, setHeroes] = useState([]);

    const fetchHeroes = async () => {
        const heroes = [];
        for (let id = 455; id < 460; id++) {
            const hero = await (await fetch(
                "https://www.superheroapi.com/api.php/3061607853876230/"
                + id)).json();

            console.log(hero);
            heroes.push(hero);
            console.log(heroes);
        }
        const listItems = heroes.map((hero) =>
            <div key={hero.id.toString()} className="box">

                <li  className="listed-text" >
                    <h1>- {hero.name} -</h1>
                    <h2>{hero.biography["full-name"]}</h2>
                    <h3>{hero.work.occupation}</h3>
                    <h4>{hero.appearance.gender}</h4>
                    <a href={'/hero/' + hero.id}>
                        <Link to={'/hero/' + hero.id}>More info</Link>
                    </a>
                </li>
                <img className="cover"></img>

            </div>

        );

        try {
            ReactDOM.render(
                <ul className="flex-container">{listItems}</ul>,
                document.getElementById('heroes')
            );
        } catch {
        }
    };

    return (

        <div id="heroes">

        </div>
    );
}

export default Heroes;
