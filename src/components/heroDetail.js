import React, {useState, useEffect} from 'react';
import '../style/App.css';

function HeroDetail({match}) {
    useEffect(() => {
        fetchHero();
        console.log(match)
    }, []);

    const [hero, setHero] = useState({});

    const fetchHero = async () => {
        const hero = await (await fetch(
            'https://www.superheroapi.com/api.php/3061607853876230/' + match.params.id
        )).json();

        setHero(hero);
        console.log(hero)
    }

    return (
        <div>
            <h1>
                {hero.name}
            </h1>
        </div>
    );
}

export default HeroDetail;
